# Chrome Extension - Trello Group-by Filtering

## Setup Flow

### Public - Chrome Web store

Search and install [Trello Group-by Filtering]()

### Offline

1. Download [chrome_extnsions_trello_groupby_filtering.zip](chrome_extnsions_trello_groupby_filtering.zip) and unzip files
2. Open Chrome and goto [chrome://extensions/](chrome://extensions/) and click `Load Unpacked`

    ![load_unpacked.jpeg](images/load_unpacked.jpeg)

3. Choose `chrome_extnsions_trello_groupby_filtering` directory

## How to use it

Click it when you focus the page with check box on Jenkins.

![screenshot.jpeg](images/screenshot.jpeg)