(async () => {
  const storageName = 'chrome-extension-trello';
  let localStorageData = {};
  await setData(storageName);
  await rendering();

  // addEventListener
  document.getElementById("trelloGroupItems").addEventListener("click", function (e) {
    if (e.target) {
      const data = e.target.dataset.members.split(',');
      let value = '';
      data.forEach(element => {
        if (value !== '') {
          value += ',';
        }
        value += `member:${element}`
      });
      //https://trello.com/b/gMVpiMEW/development-delivery?menu=filter&filter=member:blackietsai,member:damienhuang1
      chrome.tabs.query({
        active: true,
        lastFocusedWindow: true
      }, function (tabs) {
        // and use that tab to fill in out title and url
        let tab = tabs[0];
        let url = new URL(tab.url)

        let modifyUrl = `${url.protocol}//${url.host + url.pathname}?filter=${value}`;
        // window.history.pushState({},"",modifyUrl);
        chrome.tabs.update(tab.id, {
          url: modifyUrl
        });
      });
    }
  });

  document.getElementById("reset").addEventListener("click", function (e) {
    if (e.target) {
      reset(storageName);
      const rootSelector = '#trelloGroupItems';
      let appRoot = document.querySelector(rootSelector);
      while (appRoot.lastElementChild) {
        appRoot.removeChild(appRoot.lastElementChild);
      }
      rendering();
    }
  });

  document.getElementById("export").addEventListener("click", function (e) {
    if (e.target) {
      const data = getFromStorage(storageName);
      exportData(data);
    }
  });

  // functions
  async function rendering() {
    const rootSelector = '#trelloGroupItems';
    let appRoot = document.querySelector(rootSelector);
    const localStorageData = await getFromStorage(storageName);
    localStorageData.forEach(dataItem => {
      var item = document.createElement('li');
      item.className = "groupItem";
      item.innerHTML = dataItem.name;
      item.dataset.members = dataItem.members;
      appRoot.appendChild(item);
    });
  }

  async function setData() {
    // Save it using the Chrome extension storage API.
    chrome.storage.sync.set({
      'chrome-extension-trello': [{
          "name": "presentation",
          "members": ["blackietsai", "crystaltan0702"]
        },
        {
          "name": "gaming",
          "members": "gaming"
        }, {
          "name": "accounts",
          "members": "accounts"
        }, {
          "name": "reporting",
          "members": "reporting"
        }, {
          "name": "sre",
          "members": "sre"
        },
      ]
    }, function () {
      console.log('Settings saved');
    });
  };

  function exportData(data) {
    alert(JSON.stringify(data));
    const blob = new Blob([JSON.stringify(data)], {
      type: "application/json"
    });
    var url = URL.createObjectURL(blob);
    chrome.downloads.download({
      url: url,
      filename: "trello_groupFiltering_export.json"
    });
  }

  function reset() {
    // Save it using the Chrome extension storage API.
    chrome.storage.sync.set({
      'chrome-extension-trello': []
    }, function () {
      console.log('reset');
    });
  }

  async function getFromStorage(key) {
    return new Promise((resolve, reject) => {
        chrome.storage.sync.get(key, resolve);
      })
      .then(result => {
        if (key == null) return result;
        else return result[key];
      });
  }

})();